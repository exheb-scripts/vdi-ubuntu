# vdi-ubuntu

Préparation et configuration d’un poste de travail virtuel **Ubuntu** destiné à
devenir un modèle (*template*) pour le déploiement de pools **VDI VMware Horizon**.
Cette note recueille les éléments utils à la préparation du système
d’exploitation Ubuntu, la configuration des différents services et
l’installation de l’agent Horizon. Le déploiement d’un pools n’est pas traité.

Cette note est référencée par le [logiciel de travail collaboratif](https://confluence.epfl.ch:8443/pages/viewpage.action?pageId=88114281) de la VPSI.
Il faut égallement prendre en compte les informations dispenséées aux
[responsables de salle de cours VDI](https://wiki.epfl.ch/itvdi-docs) et
maintenir à jour les
[informations d'authentification](https://wiki.epfl.ch/itvdi-docs/creds)
(les mots de passe).

Étapes prévues
1.  Installer une machine virtuelle Ubuntu
2.  Configuration de base, setup-1-base
3.  Mise en place du SSSD, setup-2-sssd
4.  Mise en place SSL/TLS, AutoFS/NFSv4, setup-3-autofs
5.  Mise en place de la configuration des utilisateurs, setup-4-roaming
6.  Derniers détails et point final

Autres détails
- Ajouter manuellement les queues d'impression myPrint
- Ubuntu AD (aka Active Directory) avec seulement l'identification
  et l'authentification

#### 1. Installer une machine virtuelle Ubuntu
L'installation de la machine virtuelle (VM) Ubuntu sur VMware vSphere --- ou
VMware Fusion --- demande l'image ISO qui va bien (au moins):
* ubuntu-20.04-desktop-amd64.iso
* ubuntu-18.04.2-desktop-amd64.iso
* ubuntu-16.04.4-desktop-amd64.iso

Pour les versions les plus récentes, se rendre directement sur le portail
[Ubuntu](http://releases.ubuntu.com/):
* [Ubuntu Release 20.04](http://releases.ubuntu.com/20.04/) et 64-bit PC (AMD64)
_desktop image_)
* [Ubuntu Release 18.04](http://releases.ubuntu.com/18.04/) et 64-bit PC (AMD64)
_desktop image_)
* [Ubuntu Release 16.04](http://releases.ubuntu.com/16.04/) et 64-bit PC (AMD64)
_desktop image_)

VMware Fusion Settings (minimal)
```
  Processors & Memory
    Processors
      2 processor cores
      2 GB memory
    Hard Disk (SCSI)
      25 GB (18.04), 20 GB (16.04)
  Advanced options
    [x] Enable hypervisor…
    [x] Enable code profiling…
    [x] Enable IOMMU…
```
> Notes:
> 
> *  Lors de l'installation Ubuntu 20.04, le paramètrage du clavier n'est pas
>    pris en compte. Par conséquent, attention au mot de passe choisi !
> *  Depuis Ubuntu 20.04, 2 cœurs CPU sont devenus nécessaire.
> *  Depuis Ubuntu 18.04.4, 4 GB memory sont recommendés (avant 2 GB).
> *  Avec _vSphere_ envisager 64 Mo pour l'affichage. Modifier les
>    paramètres, Paramètre virtuel, Carte vidéo, Mémoire vidéo totale.
> *  25 GB pour Ubuntu 18.04 et 20 GB pour Ubuntu 16.04 représentent l'espace de
>    stockage minimum pour l'utilisation du système d'exploitation. Pour un
>    usage destiné à un environnement de travail, entre 50 et 55 GB d'espace de
>    stockage est plus réaliste.

Spécificités d'installation Ubuntu 18.04.2
```
GNU Grub
  * Install Ubuntu
(1/7) Welcome
  English
  Install Ubuntu
(2/7) Keyboard layout
  Switzerland - French(Switzerland)
    --> Retirer English (US) une fois le système installé.
(4/7) Updates and other software
  What apps would you like to install to start with?
  (*) Normal Installation
      Web browser, utilities, office software, games, and media players.
  Other Options
  [x] Dowload updates while installing Ubuntu
    --> Recommandé !
(5/7) Installation type
  This computer currently has no detected operating systems. What would you like to do?
  (*) Erase disk and install Ubuntu
      Warning: This will delete all your programs, ...
(6/7) Where are you?
  Zurich
(7/7) Who are you?
  Your name: Sys. Admin.
  Your computer's name: vsphere-sysvm-a
  Pick a username: system
  (*) Require my password to log in
```

Spécificités d'installation Ubuntu 16.04.5
```
   GNU Grub
     *Install Ubuntu
   Welcome
     English
   Preparing to install Ubuntu
     [x] Download updates while installing Ubuntu
       --> Recommandé !
     [ ] Install third-party software ...
   Installation type
     (*) Erase disk and install Ubuntu
   Where are you?
     Zurich
   Keyboard layout
     Switzerland - French (Switzerland)
     --> Retirer English (US) une fois le système installé.
   Who are you?
     Your name: Sys. Admin.
     Your computer's name: fusion-sysvm/vsphere-sysvm
       alternate hostname: fusion-sysvm-a, fusion-sysvm-b, fusion-sysvm-a1
     Pick a username (user name): system
```

#### 2. Configuration de base, setup-1-base
Les éléments de base sont mis en place pour obtenir une système à jour et
fonctionnel:
*  Installation des packages nécessaires
*  Correction des locales
*  Modification /etc/hosts (pour Horizon) et configuration NTP
*  System Security Services Daemon (SSSD)
*  Configuration de l'espace de travail de l'utilisateur system
*  Création du _Welcome message_
*  Reconfiguration des clés SSH de la machine
*  Mise à jour avec tous les correctifs du jour
*  Installation du certificat EPFL

> Recommandation: Mettre à jour le système d'exploitation avec
>                 _Software Updater_
>                 et redémarrer avant de lancer le script de configuration
>                 de base.

Lancer les commandes suivantes depuis un terminal pour récupérer les fichiers de
préparation du modèle (template):
```
# apt --yes install git-core
% git clone https://gitlab.epfl.ch/exheb-scripts/vdi-ubuntu.git
% cd vdi-ubuntu
% cp .cshrc ..
% cp .vimrc ..
% cp .gitconfig ..
```
En vue d'attribuer une identité visuelle au bureau du compte de gestion,
l'arrière plan est modifié.
```
% cp Pictures/cells_grid_light_texture_45026_1920x1080.jpg ../Pictures
% dconf write /org/gnome/desktop/background/picture-uri \
  "'file:///localhome/system/Pictures/cells_grid_light_texture_45026_1920x1080.jpg'"
```
Éventuellement changer le nom de la machine et lancer le premier script:
```
# hostnamectl set-hostname fusion-sysvm-a3
# cd Template
# ./setup-1-base |& tee setup-1-base.log
```
> Note:
> 1. L'installation des packages peut être momentanément indisponible.
> 2. La mise à jour vers `libssl1.1:amd64` nécessite une intervention mauelle.

Après le redémarrage, vérifier qu'il n'y a plus de mise à jour en suspens
avec l'application _Software Updater_. Le cas échéant appliquer les mises à jour
et redémarrer la machine.

#### 3. Mise en place du SSSD, setup-2-sssd
Ajoute le nom de la machine comme objet ordinateur (_computer object_) à
l'annuaire AD (aka Active Directory) depuis un compte d'administration
(_AD SCIPER_): `OU=intranet.epfl.ch, DIT-Services Communs, VDI, StudentVDI, VPSI`.

Comptes d'administration de l'annuaire (voir ailleurs pour les mots de passe):
*  `itvdi-ad-user-test`, pour la phase de test
*  `itvdi-ad-user`, pendant l'exploitation

Liste des fichiers à modifier pour au moins obtenir l'identification `UID/GID` et
l'authentification Kerberos. Les modifications sont réduites au stricte minimum.
*  /etc/hosts
*  /etc/resolv.conf
*  /etc/NetworkManager/NetworkManager.conf
*  /etc/realmd.conf
*  /etc/idmapd.conf
*  /etc/krb5.conf
*  /etc/samba/smb.conf
*  /etc/sssd/sssd.conf (créé)

Lancer le second script (toujours depuis un terminal):
```
  % cd $HOME/vdi-ubuntu/Template
  # ./setup-2-sssd |& tee setup-2-sssd.log
```

Vérifications de la bonne tenue de la nouvelle configuration, sans oublier,
après le redémarrage, de lancer les commandes **kinit** et **net ads join** selon
la syntaxe précisée dans le message à la fin ou dans le journal
`$HOME/vdi-ubuntu/Template/setup-2-sssd.log`, avant de relancer le service sssd.
> Note: En cas d'erreur du DNS suite à la commande **net ads join**, donner un
> nom à la machine qui n'est pas présent dans le DNS.

La machine est-elle jointe au domaine, quel est son statut ?
```
# net ads testjoin -k
Join is OK

# net ads status -k
...
cn: FUSION-SYSVM-A1
distinguishedName: CN=FUSION-SYSVM-A1,OU=Test,OU=VPSI,OU=StudentVDI,OU=VDI,OU=DIT-Services Communs,DC=intranet,DC=epfl,DC=ch
...
name: FUSION-SYSVM-A1
...
```
Recherche de l'objet machine fusion-sysvm-a1 dans AD.
```
# ldapsearch -LLL -H ldap://intranet.epfl.ch -x -D 'itvdi-ad-user-test' -w '{ password itvdi-ad-user-test }' -b 'OU=Test,OU=VPSI,OU=StudentVDI,OU=VDI,OU=DIT-Services Communs,DC=intranet,DC=epfl,DC=ch' cn=fusion-sysvm-a1 dn servicePrincipalName
dn: CN=FUSION-SYSVM-A1,OU=Test,OU=VPSI,OU=StudentVDI,OU=VDI,OU=DIT-Services Co
 mmuns,DC=intranet,DC=epfl,DC=ch
servicePrincipalName: HOST/fusion-sysvm-a1.intranet.epfl.ch
servicePrincipalName: HOST/FUSION-SYSVM-A1
```
Trouver l'enregistrement d'un utilisateur. Identification et authentification
d'un utilisateur.
```
# getent passwd <user name>
# id -u <user name>
% su <user name>
```

#### 4. Mise en place SSL/TLS, AutoFS/NFSv4, setup-3-autofs
Ajoute de nouveau paquetages relatifs aux nouveaux services engagés, et
les configure.
*  Installation des packages nécessaires
*  Autoriser l'accès aux comptes du domaine AD (aka Active Directory)
*  Autorise le groupe itvdi-admins à administer la machine
*  Déplace le dossier de départ de Sys. Admin. sous /localhost
*  Retarde le démarrage du service autofs (le service sssd s'annonce prêt alors
   qu'il ne l'est pas. Probablement un bug sur Ubuntu 16.04)

Liste des fichiers à modifier
*  /etc/dhcp/dhclient.conf
*  /etc/hosts
*  /etc/idmapd.conf
*  /etc/sssd/sssd.conf
*  /etc/pam.d/common-session
*  /etc/default/nfs-common
*  /etc/sudoers
*  /etc/ldap/ldap.conf
*  /etc/autofs_ldap_auth.conf
*  /etc/autofs.conf
*  /etc/auto.master
*  /etc/auto.home
*  /etc/nsswitch.conf
*  /etc/sssd/sssd.conf
*  /etc/init.d/autofs
*  /localhome

Lancer le troisième script:
```
% cd $HOME/vdi-ubuntu/Template
# ./setup-3-autofs |& tee setup-3-autofs.log
```

Après le redémarrage, le dossier de départ (*Home Directory*) de l'utilisateur
Sys. Admin. est déplacé sous `/localhost`.

Vérifications du bon fonctionnement des services nouvellements engagés:
```
% su - <user name>
```
> Le dossier de départ de l'utilisateur est monté sous /home/<user name>.
> Les services AutoFS et NFSv4 sont mis à contribution.

Quelques commandes supplémentaires de vérification
```
% kinit <user name>
% ldapsearch -LLL cn=fabbri dn
% ldapsearch -LLL -s sub cn=fabbri
% ldapsearch -s sub "(&(objectClass=automount)(automountkey=fabbri))"
% ldapsearch automountkey=fabbri
% ldapsearch -x -D "fabbri@intranet.epfl.ch" -b "dc=intranet,dc=epfl,dc=ch" -H ldaps://ad1.intranet.epfl.ch -W automountkey=fabbri
Enter LDAP Password: <fabbri's password>
% ldapsearch -x -Z -D "fabbri@intranet.epfl.ch" -b "dc=intranet,dc=epfl,dc=ch" -H ldap://ad1.intranet.epfl.ch -W automountkey=fabbri
Enter LDAP Password: <fabbri's password>

% ldapsearch -s sub "(&(objectClass=automount)(automountkey=fabbri))" | & \
    awk '/^name/ {printf "%s\t",$2}/^automountInformation/ {printf "%s %s",$2,$3; getline;printf "%s\n",$1}'

% sudo klist -k | grep `hostname` | awk '{print $2}' | head -n 1
host/fusion-sysvm-a1.intranet.epfl.ch@INTRANET.EPFL.CH
```

#### 5. Mise en place de la configuration des utilisateurs, setup-4-roaming
Désactive l'automontage des dossiers de départ des usagers avant d'installer les
éléments nécessaire à epfl_roaming.
* Désengage le service d'automontage (`autofs`)
* Désactive le peuplement automatique d'un nouveau dossier de départ
* Plante les racines epfl_roaming avant de démarrer son service
* Ajoute `fuse-ext2` pour disposer d'un système de fichier conforme POSIX 

> Note: Le modèle poste de travail virtuel Ubuntu (VDI) ajoute quelques
> fonctionnalités à l'originel
> [epfl_roaming_v2](https://gitlab.epfl.ch/bancal/epfl_roaming_v2)
> (Samuel Bancal). Ces différences sont notables dans le fichier de
> configuration `epfl_roaming.conf` et au niveau du code `epfl_roaming.py`.

Liste des fichiers à modifier ou ajouter
* /etc/pam.d/common-session
* /usr/local/bin/manage_cred.py
* /usr/local/bin/epfl_roaming.py
* /usr/local/lib/manage_cred/ext_epfl_roaming.py
* /usr/local/etc/epfl_roaming.conf
* /etc/skel/.config/autostart/epfl_roaming.desktop
* /etc/systemd/system/epfl_roaming_on_shutdown.service
* /etc/pam.d/common-auth
* /usr/local/[share/man/man1,bin,lib/pkgconfig,lib/umview]/{fuse-ext2*|umfuseext2*}
* /etc/fuse.conf

Lancer le quatrième script:
```shell
% cd $HOME/vdi-ubuntu/Template
# ./setup-4-roaming |& tee setup-4-roaming.log
```

#### 6. Derniers détails et point final
_En cours de rédaction_.

* Installation de VMware Horizon Agent et script de couplage (_join_)
  à Active Directory
* Corriger les PAMs (modifié par l'installation l'agent Horizon View)
* Désactiver la liste des utilisateurs au login screen
* Supprime la boîte de dialogue de confirmation des actions _logout_, _restart_
  et _shutdown_
* Ajouter manuellement les queues d'impression myPrint
* La machine quitte le domaine AD (aka Active Directory), son objet machine est
  supprimé (inverse du _join_).
* Point final message du jour (motd)

Liste des fichiers à modifier ou ajouter
* /etc/dconf/profile/gdm
* /etc/dconf/db/gdm.d/00-login-screen
* /etc/vmware/viewagent-custom.conf
* /opt/VPSI/scripts/join-only-once.sh
* /etc/pam.d/common-auth
* /etc/pam.d/common-session
* /etc/motd

Installation de VMware Horizon Agent. Auparavant, se procurer le paquet depuis
[dépôt](\\sdsdata\pl-dit-vdi-rep-01-t5\View 7.8).
```shell
% mkdir $HOME/vdi-viewagent
% cd $HOME/vdi-viewagent
% scp system@podd.epfl.ch:"Desktop/VDI\ Horizon\ Ubuntu/2019-20/VMware-horizonagent-linux-x86_64-7.8.0-*.gz" .
% tar xf VMware-horizonagent-linux-x86_64-7.8.0-*.tar.gz
% cd VMware-horizonagent-linux-x86_64-7.8.0-12610615
# ./install_viewagent.sh -A yes
...
Installation done
# systemctl status viewagent --no-pager
```

Lancer le cinquième script:
```shell
% cd $HOME/vdi-ubuntu/Template
# ./setup-5-onemorething |& tee setup-5-onemorething.log
```

Dernières étapes manuelle avant la qualification et la libération du modèle
Ubuntu en vue de son déploiement sous forme de pool VDI VMware Horizon:
1. Ajouter les queues d'impression myPrint.
2. Supprimer l'objet _computer_ du domaine Active Directory.
   ```shell
   # net ads testjoin
   Join is OK
   # kinit itvdi-ad-user-test
   Password for itvdi-ad-user-test@INTRANET.EPFL.CH:
   # net ads leave -k
   Deleted account for 'VSPHERE-SYSVM-A' in realm 'INTRANET.EPFL.CH'
   ```
3. Qualifier le modèle Ubuntu dans le message du jour (qualificatifs: _current_,
   _stable_ et _release_) et éventuellement aussi l'enrôlement au royaume
   Kerberos.
   ```shell
   # vim /etc/motd
   Ubuntu 18.04.3 LTS
   2019.08.30 stable
   VMware View Agent Engaged
   
   # vim /etc/realmd.conf
   os-name = Ubuntu (VDI)
   os-version = 18.04.3 2019-08-30 stable
   ```
4. Purge le dossier de départ _system_ des dossiers et des fichiers qui sont
   inutiles et des journaux du système qui n'ont plus cours (/var/log).
   ```
   # $HOME/vdi-ubuntu/scrupts/purge
   ```
5. Éteindre la machine (_power Off_)
6. Libérer le modèle Ubuntu en le clonant vers le dossier _Base Images_
   depuis vSphere Client.

----

#### Ajouter manuellement les queues d'impression myPrint

> Impression [myPrint](https://myprint.epfl.ch): Configuration des queues
> d'impression EPFL-BW et EPFL-Color, repectivement en niveau de gris et
> en couleur.
> EPFL-BW est la queue par défaut ([CUPS](http://localhost:631/), System Settings, Hardware, Printers).
>
>      % curl -O https://printepfl.epfl.ch/PPD-C5560i-color-EN.PPD
>      % curl -O https://printepfl.epfl.ch/PPD-6555i-bw-EN.PPD
>
>      % shasum *.PPD
>      e7a523396831b8a93d6f58f031ffcc9125898751  PPD-6555i-bw-EN.PPD
>      b9e6b81e31294931b76b6dcf1d7ca6c1fdd9b059  PPD-C5560i-color-EN.PPD
>
>      # mv *.PPD /usr/share/cups/model
>
> Depuis le portail CUPS local de la machine, [http://localhost:631/](http://localhost:631/):
>
>      Administration
>        Printers
>          Add Printer
>            Authentication Required
>              User Name: system
>              Password: <mot de passe>
>            Other Network Printers: (*) LPD/LPR Host or Printer
>              Continue
>            Connection: lpd://printepfl1.epfl.ch/SecurePrint-BW
>                        lpd://printepfl1.epfl.ch/SecurePrint-Color
>              Continue
>            Name: EPFL-BW
>            Description: Pool d'impression EPFL en niveau de gris
>            Locations: EPFL
>              Continue
>            Or Provide a PPD File: PPD-6555i-bw.ppd
>              Add Printer
>          Set Printer Options
>            Perforateur: (*) Désactivé
>              Set Default Options
>
> ATTENTION: Attendre un peu pour obtenir un retour d'information !
> 
> Sélection de l'authentification Kerberisée:
>
>      Administration
>        Server
>          [*] Use Kerberos authentication
>        Change Settings

#### Ubuntu AD (aka Active Directory) avec seulement l'identification et l'authentification

> Configuration minimum, Ubuntu 18.04.3
>
>      # apt install -y krb5-user samba sssd chrony
>      # vim /etc/samba/smb.conf
>      # Supprimer la ligne du workgroup et ajouter les lignes suivantes
>          workgroup = INTRANET
>          client signing = yes
>          client use spnego = yes
>          kerberos method = secrets and keytab
>          realm = INTRANET.EPFL.CH
>          security = ads
>      # vim /etc/sssd/sssd.conf
>      [sssd]
>      services = nss, pam, ssh, autofs, pac
>      config_file_version = 2
>      domains = intranet.epfl.ch
>      # default_domain_suffix = intranet.epfl.ch
>      override_space = _
>  
>      [domain/INTRANET.EPFL.CH]
>      id_provider = ad
>      auth_provider = ad
>      chpass_provider = ad
>      access_provider = ad
>      enumerate = False
>      krb5_realm = INTRANET.EPFL.CH
>      ldap_schema = ad
>      ldap_id_mapping = True
>      cache_credentials = True
>      ldap_access_order = expire
>      ldap_account_expire_policy = ad
>      ldap_force_upper_case_realm = true
>      fallback_homedir = /home/%d/%u
>      default_shell = /bin/false
>      ldap_referrals = true
>      use_fully_qualified_names = False
>  
>      [nss]
>      memcache_timeout = 3600
>      override_shell = /bin/bash
>  
>      # chown root:root /etc/sssd/sssd.conf
>      # chmod 600 /etc/sssd/sssd.conf
>  
>      # vim /etc/krb5.conf
>      [libdefaults]
>              default_realm = INTRANET.EPFL.CH
>              rdns = false
>  
>      # kinit itvdi-ad-user-test
>      # net ads join -k createupn=host/hub@INTRANET.EPFL.CH createcomputer='OU=Test,OU=VPSI,OU=StudentVDI,OU=VDI,OU=DIT-Services Communs,DC=intranet,DC=epfl,DC=ch' osName='ubuntu (VDI)' osVer='18.04.3 2019-09-11 current'
>      # net ads join -k
>      # net ads testjoin -k
>  
>      # vim /etc/pam.d/common-session
>      # Ajouter la ligne à la fin
>      session optional        pam_mkhomedir.so
>  
> Relancer les services concernés
>
>      # systemctl restart smbd.service nmbd.service
>      # systemctl restart sssd.service
>  
> Vérifier le comportement
>
>      # getent passwd fabbri@intranet.epfl.ch
>      # su - fabbri@intranet.epfl.ch
>  
> Redémarrer le système.

----

| révision | création |
| ------:|------:|
| 21 février 2020 | 28 février 2019 |
