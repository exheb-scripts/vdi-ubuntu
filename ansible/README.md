[comment]: # (                                      révision du 9 décembre 2024)
[comment]: # (                                                   7 février 2024)
[comment]: # (                                        fichier ansible/README.md)

# Exécuter le premier Playbook *Ansible*
A l'issue de cette procédure qui consiste à exécuter le premier Playbook
*Ansible*, le Template Ubuntu recevra la mise à jour qui lui convient composée
des éléments suivants:

- Ajoute une liste de logiciels minimum
- Efface les fichiers journaux
- Les fichiers journaux EPFL roaming sont vidés
- Mise à jour du système d'exploitation (Ubuntu)
- Vérifie si un redémarrage du système d'exploitation est nécessaire (suite à la
  mise à jour)
- Redémarrage du système d'exploitation si c'est nécessaire

## Lancer le Playbook sur un Template Ubuntu
Choisir le Template Ubuntu qui convient dans le dossier [MA/INJ]/Templates et
lancer l'action Clone-->Clone to Virtual Machine... depuis *vSphere Client*,
pour obtenir un *Clone d'Ubuntu*.

Lancer l'action Power-->Power On pour démarrer le *Clone Ubuntu*, et attendre
que l'adresse IP s'affiche dans le fenêtre principale de *vSphere Client*.

Dans l'environnement Ansible, copier l'adresse IP du *Clone Ubuntu* dans le
fichier *ansible/hosts* en regard à la variable *sysvm* (troisième tiers du
fichier).

	% vim ansible/hosts
	sysvm	ansible_host=1.2.3.4	# A travers de Firewall

Toujours depuis l'environnement Ansible, copier le script *ansible/cmadm-user*
sur le *Clone Ubuntu* avant le l'exécuter.

	% cd .../ansible
	% scp -i ~/.ssh/VDI-id_ed25519 cmadm-user system@1.2.3.4:
	% ssh system@1.2.3.4
	% sudo -s
	[sudo] password for system: 
	# ./cmadm-user

Afin d'ajouter un utilisateur administratif et s'assurer de quelques détails,
lancer le playbook qui va bien depuis l'environnement Ansible.

	% ansible-playbook --inventory hosts nodes-ubuntu/add-cmadm-user.yaml --limit sysvm

Finalement lance le playbook de mise à jour du système d'exploitation qui
l'objectif de cette procédure.

	% ansible-playbook --inventory hosts nodes-ubuntu/update-check.yaml --limit sysvm

----

| révision | création |
| ------:|------:|
| 9 décembre 2024 | 7 février 2024 |
