#
#             title: csh/tcsh resource script
#     creation date: 22 mars 2004
# modification date: 4 septembre 2019
#           version: 1.3.42
set     cshrcVersion=1.3.42
#            author: Fabbri Pascal @ DIT - Services de base (DIT-SB)
#             email: pascal.fabbri@epfl.ch
#         file name: $HOME/.cshrc
#       description: This file is read at beginning of execution
#                    by each shell
#

umask 022

if ( -e /usr/bin/uname ) then
  set UName=/usr/bin/uname
else if ( -e /bin/uname ) then
  set UName=/bin/uname
else
  /bin/echo "uname command not found \!"
  exit 1
endif

set OSName=`${UName} -s`
set ZoneName="global"

switch ($OSName)
  case 'SunOS':
    setenv LC_COLLATE C
    set path=(/usr/bin /usr/ucb /usr/sbin /usr/openwin/bin /usr/dt/bin)
    set path=($path /usr/sfw/bin /usr/sfw/sbin $HOME/bin $HOME/scripts)
    #
    # Manual pages
    #
    setenv MANPATH /usr/share/man:/usr/sfw/share/man:/usr/openwin/share/man
    setenv MANPATH ${MANPATH}:/usr/dt/share/man:/usr/java/man:${HOME}/man
    #
    # Zone
    #
    if ( -e /usr/bin/zonename ) then
      set ZoneName=`/usr/bin/zonename`
    endif
    #
    # Domain DNS
    #
    set DOMAIN=`/usr/sbin/host localhost | awk '{print $1}'`
    set DOMAIN=`echo ${DOMAIN} | awk 'BEGIN {FS="."} {print $2"."$3}'`
    breaksw
  case 'FreeBSD'
    setenv LC_ALL en_US.ISO_8859-15
    set path=(/sbin /bin /usr/sbin /usr/bin /usr/games /usr/local/sbin)
    set path=($path /usr/local/bin /usr/X11R6/bin $HOME/bin $HOME/scripts)
    setenv MANPATH `manpath`
    breaksw
  case 'Darwin':
    setenv MANPATH /usr/share/man:/usr/local/share/man:/usr/X11/man:/usr/local/cos/share/man
    # Une autre m�thode:
    #   setenv MANPATH `manpath`
    #   setenv MANPATH ${MANPATH}:
    #
    # On a le PATH suivant:
    # /usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin:/usr/X11/bin
    set path=($path $HOME/bin $HOME/scripts)
    set path=($path /usr/local/cos/bin)
    setenv CLICOLOR
    # Xcode command line tools pr�sent ou pas.
    xcode-select --print-path >& /dev/null
    if ( ! $status ) then
      set Xcodepath = `xcode-select --print-path`
      setenv MANPATH ${MANPATH}:${Xcodepath}/usr/share/man
      unset Xcodepath
    endif
    #
    breaksw
  case 'Linux':
    set path=(/bin /usr/bin /sbin /usr/sbin /usr/X11R6/bin )
    set path=($path $HOME/bin $HOME/scripts)
    #
    # Manual pages
    #
    setenv MANPATH /usr/share/man
    setenv MANPATH ${MANPATH}:/usr/X11R6/man
    breaksw
  default:
    /bin/echo "Another day in paradise \!"
    breaksw
endsw

if ( $?prompt ) then
        set history=(20480 "%h %D-%W-%Y %T %R\n")
        set savehist=(40960 merge)
        set filec
	
	#
	# Quelques variables
	#
	set teTeXversion=3.0

	#
	# Prompt setting
	#
	set Site="ExHeb"
	set Color="Yes"

	if ( $USER == "root" ) then
	  set PromptChar="#"
	else
	  set PromptChar="%"
	endif

        if ( $ZoneName == "global" ) then
	  set ZoneName=""
	else
          set ZoneName="%{\033[34m%}\{z\}%{\033[0m%}"
        endif

	if ( $?tcsh ) then
	  if ( $Color == "Yes" ) then
	    if ( $USER == "root" ) then
	      set PROMPT="%{\033[32m%}${Site}%{\033[0m%}"
	      set PROMPT="${PROMPT}[%{\033[1;31m%}%n"
	      set PROMPT="${PROMPT}%{\033[1;33m%}@%m"
	      set PROMPT="${PROMPT}%{\033[0m%}${ZoneName}]"
	      set PROMPT="${PROMPT}${PromptChar} "
	      set prompt="${PROMPT}"
	    else
	      set PROMPT="%{\033[32m%}${Site}%{\033[0m%}"
	      set PROMPT="${PROMPT}[%{\033[1;33m%}%n@%m"
	      set PROMPT="${PROMPT}%{\033[0m%}${ZoneName}]"
	      set PROMPT="${PROMPT}${PromptChar} "
	      set prompt="${PROMPT}"
	    endif
	  else
	    set prompt="${Site}[%n@%m]${PromptChar} "
	  endif
	else
	  set prompt="${Site}[${LOGNAME}@`/usr/bin/uname -n`${PromptChar} "
	endif

	setenv EDITOR	vi
        setenv PAGER	'less -ims'
        setenv LESS	'-rif'

        alias dlt       '/bin/rm -rf \!*'
        alias dlf       '/bin/rm \!*'
	alias lc	'source ~system/.cshrc'
	if ( ${OSName} == 'Linux' ) then
	  alias	ls '(setenv LANG en_US.UTF-8; /bin/ls --color \!*)'
	  alias ld '(setenv LANG en_US.UTF-8; /bin/ls --color -laF\!*)'
	else
	  alias ld	'ls -laF'
	endif
	if ( ${OSName} == 'Darwin' ) then
	  alias ne 'open -a BBEdit \!*'
          set EjectAll='tell application "Finder" to eject (every disk whose ejectable is true)'
          alias ejectall 'osascript -e "$EjectAll"'
	  alias nvim '$HOME/Builds/nvim-osx64/bin/nvim \!*'
	else
          alias ne '/opt/NEdit/5.5/bin/nedit \!* &'
	endif
	alias view	'iconv -f ISO-8859-1 -t UTF-8 \!* | less'
	alias xview	'plutil -convert xml1 \!* -o - | less'
	alias dpf       'zipinfo \!*'
        alias lpf       'unzip -p \!* | more'
	alias dtt	'setenv TERM dtterm'
	# Extended ps(1) with project and zone information
	if ("`uname -rs`" == "SunOS 5.10") then
	  alias zps	'ps -eo user,pid,ppid,projid,project,zone,args \!*'
	endif
	# Place un titre sur l'ent�te d'un terminal
	alias title	'/bin/echo "\033]0;\!*\007\c"'
	# Update tcsh startup files

        if ( $?tcsh ) then
          bindkey "^W" backward-delete-word
          bindkey -k up history-search-backward
          bindkey -k down history-search-forward
          bindkey "^R" i-search-back
	  set savehist=(2048 merge)
          set correct=all
          set autolist
	else
	  set savehist=2048
        endif
	
	#
	# Encore experimental !
	#
	if ( $?tcsh ) then
	  complete acro	'p/*/f:*.{pdf,PDF}/'
	  complete cd	'p/1/d/'
	endif

	#
	# Completion definitions for Solaris zfs command
	#
	if ( $?tcsh ) then
	  if ( ${OSName} == "SunOS" ) then
	    setenv ZFS_Completion ${HOME}/.zfs.tcsh
	    if ( -e ${ZFS_Completion} ) then
	      source ${ZFS_Completion}
	    endif
	    unsetenv ZFS_Completion
	  endif
	endif

	#
	# teTeX (LaTeX)
	#
	switch ($teTeXversion)
          case '2.0.2':
	    # teTeX (LaTeX) 2.0.2 du Solaris 9 9/04 Companion CD
	    setenv TETEXDIR /opt/sfw/teTeX
	    setenv TEXINPUTS ${HOME}/LaTeX/pkgs/lettre/inputs:
	    setenv PATH ${PATH}:${TETEXDIR}/bin/sparc-sun-solaris2.9
	    setenv MANPATH ${MANPATH}:${TETEXDIR}/man
	    breaksw
	  case '3.0':
	    # teTeX (LaTeX) 3.0 compil� sous Solaris 9 9/04 avec Sun Studio 9
	    setenv TETEXDIR /opt/teTeX
	    setenv TEXINPUTS ${HOME}/LaTeX/pkgs/lettre/inputs:
	    setenv PATH ${PATH}:${TETEXDIR}/bin/sparc-sun-solaris2.9
	    setenv MANPATH ${MANPATH}:${TETEXDIR}/man
	    breaksw
	  default:
            /bin/echo "Another day in paradise \!"
            breaksw
          endsw

	# Sun Solaris Software Companion
	setenv SFW /opt/sfw
	if ( -e ${SFW} ) then
	  setenv PATH ${PATH}:${SFW}/bin
	  setenv MANPATH ${MANPATH}:${SFW}/man
	endif
	if ( $USER == "root" ) then
	  setenv PATH ${PATH}:${SFW}/sbin
	endif
	unsetenv SFW


	# Sun Studio 9 (C & C++ compilers)
	setenv Studio /opt/SUNWspro
	if ( -e ${Studio} ) then
	  setenv PATH ${PATH}:${Studio}/bin
	  setenv MANPATH ${MANPATH}:${Studio}/man
	endif
	unsetenv Studio
	
	# trafshow
	if ( $USER == "root" ) then
	  setenv MANPATH ${MANPATH}:/opt/trafshow/man
	  alias ts '/opt/trafshow/bin/trafshow \!*'
	endif

endif
