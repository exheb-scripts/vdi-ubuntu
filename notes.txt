_____
Notes prisent pendant la mise au point des scripts de préparation du modèle.

Modèle Ubuntu 18.04.5 (2021-01-14 release)                            2021-01-19
Modèle Ubuntu 18.04.5 (2020-10-06 stable)                             2020-11-04
Modèle Ubuntu 18.04.5 7.13 (2020-10-16 stable)                        2020-10-16
Modèle Ubuntu 18.04.5 (2020-10-06 stable)                             2020-10-06
Modèle Ubuntu 18.04.5 vGPU (2020-09-09 current)                       2020-09-09
Modèle Ubuntu 18.04.4 (2020-05-29 release)                            2020-06-02
Modèle Ubuntu 18.04.4 (2020-05-29 stable)                             2020-05-29
Modèle Ubuntu 18.04.4 (2020-05-15 current)                            2020-05-15

vSphere Path:
  INJ/Templates/VPSI/Ubuntu 18.04.4 (2020-05-15 current)
  INJ/Templates/VPSI/Ubuntu 18.04.4 vGPU (2020-05-15 current)
  {MA/Templates/VPSI/Ubuntu current and stable}

Lien
  Install the NVIDIA GRID vGPU Display Driver (VMware Horizon 7.12)
    https://docs.vmware.com/en/VMware-Horizon-7/7.12/linux-desktops-setup/GUID-8791F305-469B-4324-BBE3-E65E4294564B.html

Corrections et modifications en vue pour le prochain                  2019-08-28
modèle release (actuellement release & stable).           révision du 2020-04-15

Modèle Ubuntu 18.04.4 (2020-03-06 release)
- Libré, voir le message (messages.txt).

Modèle Ubuntu 18.04.3 (2020-01-27 stable)
- Livré, voir le message (messages.txt).

Modèle Ubuntu 18.04.3 (2020-01-15 current)

vSphere Path:
  INJ/Templates/VPSI/Ubuntu 18.04 (current)
  {MA/Templates/VPSI/Ubuntu current and stable}

documentation:
  https://jhrozek.wordpress.com/2015/08/19/performance-tuning-sssd-for-large-ipa-ad-trust-deployments/

À ajouter dans un des scripts .../Template/setup-*
|_ Firefox avec le champs de mot de passe lors de l'authentification, mais
   seulement dans le context VMware Horizon VDI
   (https://bugs.launchpad.net/ubuntu/ bug #1838358).
   /etc/profile.d/input-method-config.sh
   <...>
     export IBUS_DISCARD_PASSWORD_APPS="firefox"
   <...>
|_ Installation de chromium, vivaldi et opera.
|_ Préférences chromium, vivaldi et opera dans epfl_roaming.conf.

2020-01-24:
|x
 Documentation ajoutée à epfl_roaming.conf.
|x /etc/motd
|x Optimisation des performances au démarrage Ubuntu (sssd.conf(5)):
   /etc/sssd/sssd.conf
   ignore_group_members = True
   Démarre maintenant en moins de 30s (avant > 40s) sur macOS 10.14.6 &
   VMware Horizon Client 5.3
2020-01-23:
|x 4ième déployement chromium, vivaldi et opera:
|x Préférences chromium, vivaldi et opera dans epfl_roaming.conf:
   .config/opera/
   .config/chromium/
     .config/google-chrome/
   .config/vivaldi/
|x Optimisation des performances au démarrage Ubuntu (sssd-ldap(5)):
   /etc/sssd/sssd.conf
     ldap_search_base = dc=intranet,dc=epfl,dc=ch
     ldap_user_search_base
     ldap_group_search_base
     ldap_netgroup_search_base
     ldap_service_search_base
|x /etc/motd
|x Installation de chromium (247 MB), vivaldi (224 MB) et opera (229 MB):
   # apt-get install chromium-browser

   # add-apt-repository "deb [arch=i386,amd64] https://deb.opera.com/opera-stable/ stable non-free"
   # apt install opera-stable

   # wget -qO- http://repo.vivaldi.com/stable/linux_signing_key.pub | apt-key add -
   # add-apt-repository "deb [arch=i386,amd64] http://repo.vivaldi.com/stable/deb/ stable main"
   # apt install vivaldi-stable
|x Firefox Browser 72.0.1 n'a plus de problème lors de l'identification
   via « gaspar »:
   La version ibus 1.5.17-3 ajoute deux variables, IBUS_DISCARD_PASSWORD et
   IBUS_DISCARD_PASSWORD_APPS permettant de corriger le problème que rencontre
   Firefox avec le champs de mot de passe lors de l'authentification, mais
   seulement dans le contexte VMware Horizon VDI
   (https://bugs.launchpad.net/ubuntu/ bug #1838358).
   - Packages nécessaire (et leur version):
     % apt list -a ibus
     ibus/bionic-updates,bionic-security,now 1.5.17-3ubuntu5.2 amd64 [installed]
     ibus/bionic 1.5.17-3ubuntu4 amd64
   - Configuration permettant de corriger le problème:
     /etc/profile.d/input-method-config.sh
     <...>
     export IBUS_DISCARD_PASSWORD_APPS="firefox"
     <...>
   - Exemples:
     % env IBUS_DISCARD_PASSWORD=1
     % env IBUS_DISCARD_PASSWORD_APPS='firefox,.*chrome.*'
2020-01-22:
|x Firefox Browser 72.0.1 a toujours un problème lors de l'identification
   via « gaspar ».
|x 3ième déployement
|x /etc/motd
|x [ok] Force en minuscule (lowercase) le User Name (epfl_roaming).
   Corrigés: epfl_roaming.py, sssd.conf.
2020-01-21:
|x Essayer de passer outre les majuscules dans le User Name:
     /etc/sssd/sssd.conf
     [domain/INTRANET.EPFL.CH]
     # /var/log/sssd/sssd_INTRANET.EPFL.CH.log
     case_sensitive = Preserving
     debug_level = 8
|x [échec] Force en minuscule (lowercase) le User Name (epfl_roaming)
|x Second déployement
|x Réinstallation de l'agent View

2020-01-17:
|x Premier déployement (l'agent View est manquant).
|x /etc/motd
|x Update System: apt update, apt dist-upgrade, apt autoremove

2020-01-16:
|x Force en minuscule (lowercase) le User Name (epfl_roaming).
|_ ...

2020-01-15:
|x /etc/motd
|x Désactivation de la mise à jour automatique:
     Software & Updates, Settings..., Automatically check for updates
       Daily vers Never
|x Le package winbind est retiré: apt remove winbind
|x Le package zsh est ajouté: apt install zsh
|x Update System: apt update, apt dist-upgrade, apt autoremove
     --> Samba modifié pendant la mise à jour:
           /etc/samba/smb.conf
     --> PAM sont modifiés pendant la mise à jour:
           /etc/pam.d/common-{auth,account,password,session}
|x Espace de stockage de 25 à 50 GB
|x Firefox Browser 72.0.1 n'a plus de problème lors de l'identification
   via « gaspar ».

1/ Ajouter myPrint.
   Document podd.epfl.ch:Desktop/VDI Horizon Ubuntu/Install CUPS.txt

2/ Très lent à l'ouverture de session (entre 30 et 40 secondes d'écran noir,
   après s'être authentifié).
   À voir si on peut améliorer le délai d'ouverture de session en n'utilisant
   pas posixfs, par exemple.
   Temps d'ouverture de session (depuis Horizon Client 5.1, macOS):
     Sans posixfs:    ~ 40 s
     Avec posixfs:    ~ 40 s
   posixfs n'a pas d'incidence sur le délai d'ouverture de session !

3/ Les boutons « Power off » et « Restart » demandent l'authentification
   correspondant à un administrateur.
   Pour éviter de nombreux incidents d'étudients, supprimer cette possibilité.
   ** Impossible de supprimer ces boutons, ni au niveau GSettings schema files,
   ni au niveau utilisateur avec gsettings(1).
   - Avec VMware Fusion, pas de problème.
   - vSphere ou Horizon est la source du problème.
     --> Pas résolu !

4/ Les dossiers myFiles et posixfs sont à double sur le bureau.
   epfl_roaming.conf à modifier.

5/ Renseigner le mot de passe est problématique lors de l'authentification sur
   un portail Web comme ewa.epfl.ch ou tequila à travers Firefox. Le Desktop
   reste stable mais un peu ralenti pendant un instant.
   Le problème vient-il de Firefox, d'Ubuntu ou VDI ?
   - Avec VMware Fusion, même problème.
   - Avec Firefox 69.0, idem.
     --> Pas résolu !
         - Ni Opera ni Vivaldi ne rencontrent ce problème.

6/ « What's new in Ubuntu » (Welcome to Ubuntu) se présente à chaque session.
   Une ligne dconf ajoutée à epfl_roaming.conf.

7/ Documenter les directives epfl_roaming.conf.

8/ Lorsque la directive programmer (pas encore obsolète) est activée
   dans epfl_roaming.conf, epfl_roaming.py se plante en raison d'un package
   manquant (# apt install smbclient).

9/ Prévoir 55 GB d'espace de stockage pour le modèle                  2019-10-03

Déploiement du modèle (template), nommage.                            2020-05-26
Depuis vSphere Client
  Origine du modèle:
    .../Templates/VPSI/Ubuntu 18.04 (current)
      Ubuntu 18.04.4 (2020-05-15 current)
  Destination du clône:
    .../Desktop/VPSI/Ubuntu LAB
      [c¦s¦r]2020-05-15
  Cliché (snapshot)
    Snapshot de VM 26.05.2020 14:12:15
    N°1 (Horizon 7.8)
Depuis VMware Horizon 7 Administrator
  Pool
    ID: VPSI-ubt-[c¦s¦r]2020-05-15
    Display Name: Ubuntu 18.04.4 (2020-05 current)
    Description: Ubuntu 18.04.4 2020-05-15 current
  Mode d'attribution de nom: c2020-05-15-{n:fixed=2}

Déploiement du modèle (template), nommage.                            2019-08-12
    Destination du clône:
      .../Desktop/vpsi/Ubuntu 18.04.2 (current 2019-08-12)/
        c2019-08-12
    Cliché (snapshot)
      Snapshot de VM 13.08.2019 06:54:55
      VMware Horizon 7
    Pool
      ID:           VPSI-ubt-c2019-08-12
      Display Name: Ubuntu 18 c0812
      Description:  Ubuntu 18.04.2 2019-08-12 current

Matlab sur le pool IPHYS-UBUNTU (Laurent Villard)                     2019-10-25
1/ Il faut lancer matlab -softwareopengl, sinon cela fait planter la machine,
2/ même là, l'exécution de Matlab est très lente, même pour des opérations
   élémentaires.
3/ Le Property Inspector ne fonctionne pas: impossible d'éditer des objets d'une
   figure.
4/ Peut-ête faudrati-il installer une version antérieure de Matlab (Nous
   n'avons pas besoin des dernières nouveautés de la version 2019).

VMware Horizon View 7.10 pour Linux                                   2019-11-06
- View 7.10 is a long term release ans a big release for Linux
- Open VM Tools now recommended
- Ubuntu 18.04 is not yet supported for vGPU
- No ticket with GSS
- Active Directory Integration:
  - PowerBrocker Identity Services Open (PBISO) with instant Clones
    - Ubuntu 16.04 and 18.04
    - SLED/SLES 11 and 12.x
  - Samba integration for offline directory join
    - Ubuntu 16.04 and 18.04
    - RHEL 6.9 and 7.3
    - CentOS 6.9/7.3
    - SLED 11 SPA4/12
- Automated Full-Clone Desktop Pools
- Instant-Clone Floating Desktop Pools
- K Desktop Environment (KDE)
- MATE Desktop Environment on Ubuntu
- True SSO
- Schéma fonctionnel (block diagram), preparing the Linux Virtual Machine
    1. Create virtual machine and select Ubuntu as the OS type
    2. Set display and video RAM
    3. Install Guest OS Ubuntu/Suse
    4. Update (apt-get update/upgrade)
    5. Install Open-VM-Tools
    6. Install Dependency Packages for Horizon Agent for Linux
    7. Install NVIDIA vGPU Drivers
    8. Setup AD (aka Active Directory) integration
    9. Install Horizon Agent for Linux
- Horizon Agent for Linux
    https://my.vmware.com/web/vmware/downloads
- Optimizing Linux Desktops
  - Disable Compiz for better desktop performance
  - Disable unnecessary service:
    - bluetooth
    - postfix
    - netfs
    - mdmonitor
  - Make sure updates are installed
    - yum check-update
    - atp-get update

Pour éventuellement éviter une erreur de mise à jour du système       2020-01-07
(apt update, apt dist-upgrade, apt autoremove) lors du traitement du
service winbind (systemctl restart winbind.service), purger le fichier
/etc/krb5.keytab contenu par le modèle (template).

Nécessaire de comilation (en général) et supléments pour PostgreSQL   2020-04-15
# apt-get --yes install build-essential
-- Idem pour PostgreSQL
# apt-get --yes install libreadline6-dev zlib1g-dev libxml2-dev

Ajout du montage automatique AutoFS pour CIFS en parallèle à NFSv4    2020-04-08
/etc/auto.master
/cifsnas    /etc/auto.cifsnas

/etc/auto.cifsnas
#
# L'option vers (vers=2.1) n'est pas nécessaire car le client et et serveur CIFS négocient la verion
# la plus élevée, égale ou supérieur à 2.1.
#
# Ajouter une option dans la section du domaine SSSD [sssd-krb5(5)]:
# /etc/sssd/sssd.conf
#   [domain/INTRANET.EPFL.CH]
#   # Location of the user's credential cache
#   krb5_ccname_template=FILE:%d/krb5cc_%U
#
fabbri  -fstype=cifs,sec=krb5,cruid=$USER,user=$USER,gid=$GID,uid=$UID  ://files9.intranet.epfl.ch/data/&

/etc/sssd/sssd.conf
[domain/INTRANET.EPFL.CH]
krb5_ccname_template=FILE:%d/krb5cc_%U

# systemctl reload autofs.service
# mount -t cifs -t nfs4

/etc/autofs.conf --> Facultatif !
logging = verbose

Client Drive Redirection (CDR)                                        2019-09-06
Supprimer le dossier tsclient du Bureau de l'usager. De toute façon ça ne
fonctionne pas ! Activer ou pas la fonctionnalité, le dossier tsclient est
toujours présent sur le bureau.

Lorsque la fonction CDR est activée, il est possible d'accéder aux dossiers et
aux lecteurs partagés du système local. Le dossier tsclient placé sur le bureau
est utilisé. Par défaut cette fonctionnalité est active.

- Bug de la fenêtre noire (VMware View Agent 7.8)                     2020-07-02
Le problème s'est révélé lors de la mise à jour Ubunru 18.04.3 vers 18.04.4,
forcant à utiliser une version de l'agent View plus élevé que celle d'Horizon.

N° d'incident (Support Request): VMware Horizon 7.x - SR 20101048202

Ouverture et traitement de l'incident, Manuel Francisco (consultant) et
vérification finale Pascal Fabbri.

Résolution:
To fix this, please add the below configuration in the config file.

1. Take a console to the VM or ssh to the VM (with Horizon Agent 7.8 installed)
2. Change the location to the below directory
   # cd  /etc/X11/Xsession.d
3. Edit 50vmware-view-x11 file with any text editor like nano or vim and add
   the below function, save and close the file
 
if [ -n "$DISPLAY" ]; then
export DISPLAY=:${DISPLAY##*:}
fi

Note: Create a file 50vmware-view-x11 without any extension if the file does
      not exists

4. Restart the VM to take effect
5. Connect from Horizon Client. Issue should be resolved.

I have tested in my lab with your image and the issue is no longer reproduced.


- Ajouter un utilisateur pour Ansible                                 2020-06-30

--> /etc/motd
Managed Configurations by Ansible
--> /etc/ssh/sshd_config
PermitRootLogin no
LogLevel VERBOSE

# adduser --shell /bin/tcsh --system --group --disabled-password --gecos "Ansible CM Adm." cmadm
Adding system user `cmadm' (UID 127) ...
Adding new group `cmadm' (GID 131) ...
Adding new user `cmadm' (UID 127) with group `cmadm' ...
Creating home directory `/home/cmadm' ...

# mkdir ~cmadm/.ssh
# chown cmadm:cmadm ~cmadm/.ssh
# chmod go-wrx ~cmadm/.ssh
# cat <user>/id_ed25519-cmadm.pub >> ~cmadm/.ssh/authorized_keys
# chown cmadm:cmadm ~cmadm/.ssh/authorized_keys
# chmod go-r ~cmadm/.ssh/authorized_keys

# systemctl restart sshd.service
# systemctl status sshd.service

--> /etc/sudoers.d/cmadm-user
cmadm ALL=(ALL) NOPASSWD: ALL
# chmod u=r-wx,g=r-wx,o-rwx /etc/sudoers.d/cmadm-user

- Mise à jour Ubuntu sans reprendre un Template                       2020-06-02

Souvent, la mise à jour du système d'exploitation du modèle Ubuntu modifie la
disposition des PAM, conduisant à des difficultés d'identification, et altère
l'agent VMWare Horizon View installé, menant le client Horizon à afficher le
message « The display protocole for this desktop is currently not available ».

Pour remédier à cette situation, la procédure suivante permet une mise à
jour Ubuntu en minimisant le risque de perdre le protocole d'affichage et
préserver la configuration des PAM.

Résumé du déroulement de la mise à jour:

1/ Vérifier l'adéquation du nom de la machine avec son adresse IP
2/ Sauvegarder les Pluggable Authentification Modules (PAM)
3/ Appliquer la mise à jour et redémarrer le système
4/ Installer l'agent VMware Horizon View
5/ Restaurer les PAM

Procédure de mise à jour:

1/ Adresse IP
  # ip address show | grep 'inet ' | tail -1
      inet 10.93.20.231/21 brd 10.93.23.255 scope global …
  # vi /etc/hosts
  […]
  10.93.20.231  <hostname>.intranet.epfl.ch <hostname>

2/ Sauvegarde des PAM
   # cd /etc
   # tar cf $HOME/pam.d.bak pam.d
   
3/ Appliquer la mise à jour avec les moyens habituel et redémarrer le système
   # apt update
   # apt list --upgradable
   # apt --assume-yes upgrade
   # apt --assume-yes autoremove

--> Package configuration, PAM configuration. À la question:
      « Override local changes to /etc/pam.d/common-*? »,
      la réponse est « Yes ».

4/ Installation de l'agent VMware Horizon View 7.12
   % cd $HOME
   % mkdir ViewAgent
   % cd ViewAgent
   % wget https://s3.epfl.ch/13035-60ca00cc36b850573e27b2e39906e8c8/VDI/
     VMware-horizonagent-linux-x86_64-7.13.0-16944161.tar.gz
     VMware-horizonagent-linux-x86_64-7.12.0-15765535.tar.gz
     VMware-horizonagent-linux-x86_64-7.11.0-15238356.tar.gz
     VMware-horizonagent-linux-x86_64-7.8.0-12610615.tar.gz
   % tar xf VMware-horizonagent-linux-x86_64-7.12.0-15765535.tar.gz
   % cd VMware-horizonagent-linux-x86_64-7.12.0-15765535
   # ./install_viewagent.sh -A yes
   […]
   Installation done
   
   # systemctl status viewagent --no-pager

5/ Restaurer les PAMs
   # cd /etc
   # tar xpf $HOME/pam.d.bak
   # shutdown -P +1
   % exit

Le modèle Ubuntu mis à jour est prêt !

-----

- Desactiver CDR
# vim /etc/vmware/viewagent-custom.conf
CDREnable=FALSE

- Activer et configurer CDR
# vim /etc/vmware/config
cdrserver.forcedByAdmin=true
cdrserver.sharedFolders=d:\ebooks,;c:\spreadsheets,
cdrserver.permissions=R
cdrserver.logLevel=verbose

Supprimer l'affichage de la console de vSphere d'un                   2019-09-06
poste de travail Linux (sur le thème de la confidentialité)

Lorsqu'un usager se connecte à un poste de travail Linux, celui-ci peut
également être affiché dans la console vSphere de la machine virtuelle
(VM) Linux. Il est possible de configurer les machines virtuelles (VMs) pour
s'assurer que la console vSphere est vide lorsque les usagers se connectent
à leurs postes de travail.

  - Sur l'hôte ESXi, ajouter la ligne suivante au fichier vmx de la
    machine virtuelle Linux:
      RemoteDisplay.maxConnections = "0"

Ainsi, l'affichage de la console vSphere reste vide même lors d'une connexion
sur une machine virtuelle lorsque l'usager est déconnecté du poste de travail.

Volume visible sur le bureau (Desktop)                                2019-09-06
Ubuntu est passé de Unity à GNOME entre la 16.04 et la 18.04. Ainsi, un montage
(NFS, POSIXFS ou encore de type CD-ROM) sur le bureau (Desktop) en 16.04 ne
posait pas de problème, mais en 18.04 le montage se présente deux fois. Pour
éviter ce changement de comportement d'une version à l'autre, deux solutions
sont possibles:

  1) La solution naturelle est de ne pas le crocher sur $HOME/Desktop/<name>,
     mais ailleurs, par exemple dans le dossier de départ $HOME. Ainsi, Gnome
     rendra visible (virtuellement) le montage sur le bureau, mais depuis un
     shell il n'existe pas.
  2) Modifier une préférence Gnome pour que les volumes montés n'apparaissent
     pas sur le bureau:
       /usr/share/glib-2.0/schemas/org.gnome.nautilus.gschema.xml
       volumes-visible = false

Utilitaires facultatifs                                               2020-01-22 
  Multitail, AtomEditor, docky, Sublime Text.

# apt install multirail
# multitail -f epfl_roaming2.log epfl_roaming.log
# multitail -cs -f epfl_roaming2.log epfl_roaming.log

# sh -c "wget -qO - https://packagecloud.io/AtomEditor/atom/gpgkey | sudo apt-key add -"
OK
# sh -c 'echo "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main" > /etc/apt/sources.list.d/atom.list'
# apt-get update
# apt-get --yes install atom

Installation de docky
# apt --yes install docky

Installation Sublime Text 

Install the GPG key:
# wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | apt-key add -

Ensure apt is set up to work with https sources:
# apt-get install apt-transport-https

Select the Stable channel
# echo "deb https://download.sublimetext.com/ apt/stable/" | tee /etc/apt/sources.list.d/sublime-text.list

Update apt sources and install Sublime Text

# apt-get update
# apt-get install sublime-text

Pour une commande sudo depuis le dossier le HOME root                 2020-03-11
Ainsi, tcsh est le shell utilisé par root et l'historique des commandes seront
tenues par /root/.history
1/ Ajouter les préférences (au moins)
   /root/.cshrc
   /root/.vimrc
2/ Ajouter l'alias (tcsh) aux préférences de l'utilisateur de départ (system)
   alias sd "( set HOME=/root ; sudo -i tcsh )"

Locales et autre timezone                                             2020-03-11
Voir si cette méthode est plus efficace que celle déjà utilisée
# timedatectl
# locale-gen --purge "en_US.UTF-8"
# update-locale LANG=en_US.UTF-8 LANGUAGE=en_US
# dpkg-reconfigure --frontend noninteractive locales
# echo "Europe/Zurich" > /etc/timezone
# dpkg-reconfigure --frontend noninteractive tzdata

Annuaire (LDAP)                                                       2021-02-08
-- -- -- -- --
/etc/ldap/ldap.conf, nouvelle configuration possible:

BASE dc=intranet,dc=epfl,dc=ch
HOST intranet.epfl.ch

à la place de:
BASE dc=intranet,dc=epfl,dc=ch
HOST ad1.intranet.epfl.ch ad2.intranet.epfl.ch ad3.intranet.epfl.ch ad5.intranet.epfl.ch

Service Docker                                                        2021-02-08
-- -- -- -- --
Dans le cas de l'installation du service Docker et de son activation dans le
modèle Ubuntu, le déploiement des Desktops n'aboutit pas, il tombe en erreur.
Pour y remédier il y a deux solution:
1) Ajouter comme membre au groupe docker (/etc/group) un groupe
   d'utilisateurs autorisé à utiliser le service, désactiver le service Docker
   au niveau du modèle et l'activer au niveau des Desktops (déployés) lorsque
   l'utilisateur lance une session (via les PAMs) ou un autre mécanisme.
2) Au niveau du modèle ajouter le sous-réseau utilisé par les Desktops (déployés)
   avec la ligne Subnet=10.93.16.0/21 (par exemple) dans le fichier
   /etc/vmware/viewagent-custom.conf.

Installation de VMware Horizon Agent                       2019-08-15/2020-05-27
-- -- -- -- -- -- -- -- -- -- --  --
% mkdir $HOME/vdi-viewagent
% cd $HOME/vdi-viewagent
% curl -O https://s3.epfl.ch/13035-60ca00cc36b850573e27b2e39906e8c8/VDI/
    VMware-horizonagent-linux-x86_64-7.12.0-15765535.tar.gz
    VMware-horizonagent-linux-x86_64-7.11.0-15238356.tar.gz
    VMware-horizonagent-linux-x86_64-7.8.0-12610615.tar.gz
  --> Avant: % scp system@podd.epfl.ch:"Desktop/VDI\ Horizon\ Ubuntu/2019-20/VMware-horizonagent-linux-x86_64-7.8.0-12610615.tar.gz" .
% tar xf VMware-horizonagent-linux-x86_64-7.8.0-12610615.tar.gz
% cd VMware-horizonagent-linux-x86_64-7.8.0-12610615
% ./install_viewagent.sh
...
Installation done
% systemctl status viewagent
# vim /etc/vmware/viewagent-custom.conf
RunOnceScript=/opt/VPSI/scripts/join-only-once.sh
RunOnceScriptTimeout=120
SSODesktopType=UseGnomeUbuntu
OfflineJoinDomain=none

# mkdir -p /opt/VPSI/scripts
# cp .../vdi-ubuntu/scripts/join-only-once.sh /opt/VPSI/scripts
# chmod u+rwx,g-wx+r,o-rwx /opt/VPSI/scripts/join-only-once.sh
# systemctl restart viewagent
----------

Installation du pilote vGPU (NVIDIA GRID vGPU display driver)         2020-05-28
-- -- -- -- -- -- -- --  --                                  révision 2020-09-09

Modèle Ubuntu 18.04.5 et NVIDIA 450.51.05

NVIDIA graphics cards and Linux distributions that support vGPU
  https://docs.nvidia.com/grid/11.0/product-support-matrix/index.html
    Driver Package:   NVIDIA vGPU for vSphere 6.7
    Hypervisor:       ESXi 6.7 & 7.0
    Guest OS Support: Ubuntu 18.04 & 20.04 LTS (64-bit)

    NVIDIA Driver Version: 450.51.05
    Server Version Number: 11.0

  --> Notes:
        - Ubuntu 20.04 nécessite VMware vSphere ESXi 6.7 update 1.
        - En principe Horizon Agent ne DOIT PAS être installé sur la VM Linux.
        - Ne pas oublier GPU Profile setting.

1) Le modèle Ubuntu 18.04.4 (2020-05-29 release) est le point de départ pour
   constituer le modèle avec vGPU. Avec les modificiations suivantes:
   - 3 CPUs & 6 GB de mémoire, nouveau périphérique PCI NVIDIA GRID vGPU avec
     un profile GPU grid_m10-1q
   - Mise à jour Ubuntu avec les correctifs du jour (actuellement en
     Ubuntu 18.04.5). Rester vigilant avec la modifications des PAMs et de
     Horizon Agent.
   - Renseigner /etc/motd et les notes de la VM (vSphere).

1) Depuis vSphere Client (6.7), itvdivct01.epfl.ch
  1.1) Cloner le Template (sans vGPU) nommé par exemple Ubuntu 18.04.4
       (2020-05-29 release) vers Ubuntu 18.04.5 vGPU (2020-09-09 current)
       itvdivct01.epfl.ch/BM/Templates/vpsi/fabbri/master
  1.2) Dans Matériel VM ajouter le périphérique pour NVIDIA
       Matériel VM
         Ajouter un périphérique
           Périphérique PCI partagés
  1.3) Modifier le nouveau périphérique
       Nouveau périphérique PCI
         NVIDIA GRID vGPU
         Profile GPU
           grid_m10-1q
           [Réserver toute la mémoire]
        --> VMware Horizon: ne pas oublier Convertisseur 3D: NVIDIA GRID VGPU !
  1.4) Modifier les paramères…
       CPU 3 
       Mémoire 6 GB
  1.5) Dans les remarques, en tête de list, préciser (par exemple):
         Ubuntu 18.04.5 vGPU (2020-09-09)
         2020-09-09
           - Update to Ubuntu 18.04.5
           - /etc/motd
           - NVIDIA GRID vGPU (grid_m10-1q)
           - 3 CPUs, 6 GB memory
  1.6) Lancer le Desktop et continuer avec la mise à jour Ubuntu et /etc/motd
  1.7) Réaliser un déploiement et vérifier son bon fontionnement
         --> VMware Horizon: ne pas oublier Convertisseur 3D: NVIDIA GRID VGPU !
  1.6) Lancer la VM et continuer avec l'installation du pilote NVidia (plus bas)


   - Se connecter au modèle fraîchement cloné et démarré et installation des
     packages nécessaires à la compilation du noyau et du pilote NVIDIA
     % ssh system@<adresse IP>
     # apt update
     # apt-get --yes install build-essential
     # apt-get --yes install libglvnd-core-dev libglvnd-dev libglvnd0
     # apt-get --yes install lib32ncurses5 lib32z1
     # apt-get --yes install mesa-utils
     # apt-get --yes install glmark2
2) Récupération du pilote NVIDIA, compilation du noyau avec les éléments du
   pilote Mise en place du pilote NVIDIA:
   - Récupération du driver 450.51.05
   - Compilation d'un noyau (modules) avec les éléments du pilote
   - Installation du nouveau noyau
   - Configuration du service de licence

   - Récupération du driver
     % mkdir $HOME/vGPU
     % cd $HOME/vGPU
     % wget https://s3.epfl.ch/13035-60ca00cc36b850573e27b2e39906e8c8/VDI/
       NVIDIA-Linux-x86_64-450.89-grid.run
       NVIDIA-Linux-x86_64-450.51.05-grid.run

     % chmod +x NVIDIA-Linux-x86_64-450.89-grid.run
                NVIDIA-Linux-x86_64-450.51.05-grid.run

   - Compilation d'un noyau (modules) et vérifier la somme de contrôle
     après avoir stoppé X Window
     # init 3
     # sh ./NVIDIA-Linux-x86_64-450.89-grid.run --add-this-kernel
          ./NVIDIA-Linux-x86_64-450.51.05-grid.run
     ...
     -e CRC: 923356143
     MD5: 7a08b578e6efae6493f7c8998e2b4d75

     Self-extractible archive "NVIDIA-Linux-x86_64-450.89-grid-custom.run" successfully created.

     # tail -n +4 NVIDIA-Linux-x86_64-450.51.05-grid-custom.run | md5sum | cut -b-32
     7a08b578e6efae6493f7c8998e2b4d75
   - Installation du nouveau noyau
     # sh ./NVIDIA-Linux-x86_64-450.89-grid-custom.run
          ./NVIDIA-Linux-x86_64-450.51.05-grid-custom.run
       --> Notes:
           - Continuer l'installation normalement en ignorant le message « The
             distribution-provided pre-install script failed!  Are you sure you
             want to continue? ».
           - En principe les librairies de compatibilité 32-bit ne sont pas
             nécessaire.
           - Accepter la mise à jour automatique du fichier de configuration X.
   - Configuration du service de licence et vérifier l'acquisition de la licence
     # cd /etc/nvidia/
     # cp -p gridd.conf.template gridd.conf
     # vim gridd.conf
     /etc/nvidia/gridd.conf ----<
     ServerAddress=itsvdic0002.xaas.epfl.ch
     ServerPort=7070

     BackupServerAddress=itsvdic0003.xaas.epfl.ch
     BackupServerPort=7070

     FeatureType=1
     # 0: GRID Virtual Apps
     # 1: GRID vGPU
     # 2: GRID Quadro Virtual Datacenter Workstation
     # 4: NVIDIA vComputeServer
     # (All other values reserved.)
     # Please refer to the NVIDIA GRID Licensing Guide for details of the FeatureType values.

     EnableUI=TRUE
     >----

     # systemctl status nvidia-gridd
     # systemctl restart nvidia-gridd

     # grep 'License acquired successfully' /var/log/syslog
     # less /var/log/nvidia-installer.log

3) Vérification des benchmarks (GLX-Gears, GL Mark 2). Depuis le
   « GNOME Desktop Settings », Details et About.
   % glxgears
   % glmark2
     --> Notes:
         - 5mn 36s: score 14 sans licence, 60 avec licence.
   % glxinfo -B
     --> Notes:
         - Les informations les plus complètes sur le pilote, la mémoire et le
           profile NVIDIA en exploitation.
   % ubuntu-drivers devices
   % nvidia-detector
   % nvidia-smi

4) Après la réalisation du déploiement:
   - VMware Horizon: ne pas oublier Convertisseur 3D: NVIDIA GRID VGPU
   - Pas de vGPU actif sur les Desktop droit après le déploiement, voir avec
     l'acquisition de la licence.
   - Comportement général un fois le déploiement effectué
     1) Lorsque le pool est déployé, état disponible, la connexion au Desktop
        n'aboutit pas. Un connexion SSH à la VM permet de vérifier si le service
        NVIDIA Grid Daemon est actif. Il ne l'est pas !
        % systemctl status nvidia-gridd
        ...
        Active: failed
        Active: inactive (dead)
        ...
        Il faut attendre environ 5mn que la VM redémarre, pour qu'ensuite le service
        NVIDIA Grid Daemon soit actif:
        ...
        Active: active (running)
        ...
        Aucune trace du redémarrage sur l'interface VMware Horizon Administrator
     2) Quand les choses vont pour le mieux la connexion au Desktop se passe
        normalement.
     3) Lorsque la session se termine, le Desktop (VM) est détruit et un nouveau est
        rendu disponible.
        À ce deuxième cycle (voir le troisième) il n'est plus possible d'avoir une
        connexion à un Desktop.

--------------------------------------------------------------------------------

Invitation à la mise à jour                                           2020-09-29
-- -- -- -- -- -- --  -- --

Pour éviter le message d'invitation à la mise à jour sur le Desktop:
# vim /etc/update-manager/release-upgrades
Prompt=lts --> never

| Ubuntu 20.04.1 LTS Upgrade Available
|------------------------------------------------------------------
| A new version of Ubuntu is available. Would you like to upgrade?
|------------------------------------------------------------------
| [Don't Upgrade] [Ask Me Later] [Yes, Upgrade Now]

--------------------------------------------------------------------------------

Quitter le domaine AD (aka Active Directory)
-- -- -- -- -- -- -- -- -- -- -- -- -- -- --
La machine quitte le domaine AD (son objet machine est supprimé),
inverse du join.

# net ads testjoin
Join is OK
# kinit itvdi-ad-user-test
Password for itvdi-ad-user-test@INTRANET.EPFL.CH:
# net ads leave -k
Deleted account for 'VSPHERE-SYSVM-A' in realm 'INTRANET.EPFL.CH'

# vim /etc/motd
...
VMware View Agent Engaged
----------

Désactiver la liste des utilisateurs au login screen
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
Clé GSetting: org.gnome.login-screen.disable-user-list

Lorsque que la liste des utilisateurs est désactivée, l'utilisateur doit
renseigner son nom d'utilisateur et son mot de passe.
# cd /etc/dconf/profile
# vim gdm
user-db:user
system-db:gdm
file-db:/usr/share/gdm/greeter-dconf-defaults

# cd /etc/dconf/db
# mkdir gdm.d
# cd gdm.d
# vim /etc/dconf/db/gdm.d/00-login-screen
[org/gnome/login-screen]
# Do not show the user list
disable-user-list=true

# dconf update

  --> Active la clé org.gnome.login-screen.disable-user-list
  --> Crée le profile gdm dans le fichier gdm qui est la base de données dconf
  --> Crée le keyfile gdm pour l'ensemble de la machine en plaçant les
      configuration dans /etc/dconf/db/gdm.d/00-login-screen
----------

Contrôle d'accès utilisateur
-- -- -- -- -- -- -- -- -- -
Vérifier l'absence de l'option simple_allow_users (et d'autres du même
acabit) limitant l'accès uniquement à cerains utilisateurs.

/etc/sssd/sssd.conf
simple_allow_users = fabbri
----------

fuse-ext2, Produire est installer.                                    2019-01-25
-- -- -- -- -- -- -- -- -- -- -- -

% pwd
% mkdir $HOME/Builds
% cd $HOME/Builds
% git clone https://github.com/alperakcan/fuse-ext2.git
% cd fuse-ext2/
% apt search autoconf
# apt-get install autoconf
% ./autogen.sh
% ./configure
% sudo apt-get install m4 autoconf automake libtool
% sudo apt-get install libfuse-dev e2fsprogs comerr-dev e2fslibs-dev
% ./autogen.sh
% ./configure
% make
% sudo make install
% fsck.ext2 -yf $HOME/Builds/ext2-1G-disk.fs
% fuse-ext2 -o rw+,allow_other,uid=`id -u $USER`,gid=`id -g $USER` Builds/ext2-1G-disk.fs $HOME/posixfs
% cat /etc/fuse.conf
user_allow_other
% /usr/local/bin/fuse-ext2 -o rw+,allow_other,uid=`id -u $USER`,gid=`id -g $USER` Builds/ext2-1G-disk.fs $HOME/posixfs
----------

Mise à jour Ubuntu avec les correctifs de sécurité                    2020-03-02
La sécurisation des machines VDI Ubuntu commence lors de la livraison du modèle
Ubuntu. Au moment de sa livraison, le modèle Ubuntu contient déjà la mise à jour
du système Ubuntu pour les correctifs de sécurité, les corrections de bogues et
les mises à niveau des applications. Comme la mise à disposition de modèle
Ubuntu intervient entre chaque semestre, typiquement en septembre et en février,
les clones du modèle Ubuntu, qui servent au déploiement VDI, se doivent
d’appliquer les mises à jour au plus près de leur mise à disposition.
La mise à jour du système d’exploitation s’effectue soit en ligne de commande
avec apt update/upgrade ou avec l’application Software Updater (GUI). Dans les
deux cas, toutes les mises à jour sont appliquées, y compris celles de sécurité.
Pour les mises à jour de sécurité uniquement, le package unattended-upgrades
fonctionne automatiquement après sa configuration. Les correctifs de sécurité
sont automatiquement installés dès leur publication, sans intervention manuelle,
pour autant que le ou les correctifs ne nécessitent pas un redémarrage du
système, auquel cas l’accord de l’usager est demandé.
Comme la mise à jour de sécurité automatique pourraient demander l’accord de
l’usager, elle ne peut être envisagée dans un déploiement VDI.

La mise à jour de sécurité automatique est un processus qu’il faut engager et
configurer, il n’est pas présent par défaut dans le système. Dans le cas d’un
déploiement VDI, s’il est engagé, lorsqu’un correctif de sécurité nécessite le
redémarrage du système, l’usager est sollicité pour accepter ou pas le
redémarrage. Avec un refus le correctif de sécurité n’est pas installé et
l’usager peut continuer son activité. En acceptant le redémarrage le correctif
est installé avant que la session se termine, forçant l’ouverture d’une nouvelle
session à l’usager. Mais ce déroulement se reproduit en boucle puisque le modèle
utilisé pour le déploiement de clone instantané ne contient pas le correctif de
sécurité. En règle générale, un correctif de sécurité demande souvent un
redémarrage du système. L’engagement de la mise à jour automatique de sécurité
risque de produire beaucoup de sollicitations au niveau des assistants
étudiants, des responsables de salles et au final au travers d’incidents. Je ne
la recommande pas. De plus, rien ne garanti qu’un correctif de sécurité ne
reproduise pas le problème de Black Window que l’on rencontre avec le passage
à Ubuntu 18.04.4.

Quelques commandes utiles                                             2019-07-05
-- -- -- -- -- -- -- -- -

Liste des services du système
# systemctl list-units --all --type=service --no-pager

% ssh system@podd.epfl.ch 'grep itvdi-ad-user "Desktop/VDI Horizon Ubuntu/set-locale" | head -n2'

% ssh system@podd.epfl.ch
% grep itvdi-ad-user Desktop/VDI\ Horizon\ Ubuntu/set-locale | head -n2

---- fin des notes -------------------------------------------------------------
