#!/bin/sh
#
# 18 mai 2018
# révision du 5 novembre 2020
#
# mkdir -p /opt/VPSI/scripts
# /opt/VPSI/scripts/join-only-once.sh
#
# /etc/vmware/viewagent-custom.conf
# RunOnceScript=/opt/VPSI/scripts/join-only-once.sh
#
# Note: Pour copier le journal $localLog vers $remotelog, il faut des
#       des clés SSH passphrase-less.

localLog="/var/tmp/join-only-once.log"
remotelog="system@podd.epfl.ch:VDI-cours/`hostname`.log"
sshkey="/root/.ssh/lab_ed25519"
debug="yes"

date >> ${localLog}
printf "HostName: `hostname`\n" >> ${localLog}
printf "IP: %s\n" `ip addr show | egrep 'inet.*brd' | awk '{print $2}'` >> ${localLog}

/bin/rm /etc/krb5.keytab

for count in 1 2 3
do
  /usr/bin/net ads join -U itvdi-ad-user-test%{Password itvdi-ad-user-test } >> ${localLog} 2>&1
  status=$?
  if [ $status -eq 0 ]; then
    break
  fi
  sleep 2
done
printf "Retry: ${count}\n" >> ${localLog}
printf "Join exit status: ${status}\n" >> ${localLog}

if [ $status -eq 0 ]; then
  sss_cache --everything
  systemctl restart sssd
fi

# En principe autofs est désactivé
# sleep 2
# systemctl restart autofs

date >> ${localLog}
printf "join-only-once script done.\n" >> ${localLog}

# Copie le journal vers le collecteur
if [ "$debug" = "yes" ]; then
  scp -i ${sshkey} ${localLog} ${remotelog}
fi

# En cas d'échec au join, la machine est éteinte !
# Elle sera démarrée à nouveau plus tard par VMware Horizon.
if [ $status -ne 0 ]; then
  printf "System goes in power-off.\n" >> ${localLog}
  systemctl --force --message="Join to Active Directory (aka AD) failed." poweroff
  exit 1
fi

# Effacement du script lui même (ou pas) !
if [ "$debug" != "yes" ]; then
  /bin/rm /opt/VPSI/scripts/join-only-once.sh
fi

# end of script
