#!/bin/sh
#
# setup-5-onemorething
#
# VDI VMware Horizon Ubuntu 18.04.2/16.04.5
# 30 août 2019, Pascal Fabbri
# révision du 6 septembre 2019
#
# 6 septembre 2019:
# - Supprimer le dossier tsclient du Bureau de l'usager, même si cela
#   n'a aucun effet avec VMware Horizon 7.8.
#   Client Drive Redirection (CDR) /etc/vmware/viewagent-custom.conf
#
# Préalable:
# - L'agent Horizon View doit être installé.
#
# Notes:
# - Lancement du cinquème script:
#   % cd $HOME/vdi-ubuntu/Template
#   # ./setup-5-onemorething |& tee $HOME/vdi-ubuntu/Template/setup-5-onemorething.log
#
# ---- {n/5} -------------------------------------------------------------------

if [ $USER != "root" ]
then
  printf "Run as root !\n"
  exit 1
fi

cf=$HOME/vdi-ubuntu/Template/cf

# Install le script de couplage à AD (aka Active Directory)
printf "{1} Install AD binding script (join-only-once.sh)\n"
mkdir -p /opt/VPSI/scripts
cp ${HOME}/vdi-ubuntu/scripts/join-only-once.sh /opt/VPSI/scripts
chmod u+rwx,g-wx+r,o-rwx /opt/VPSI/scripts/join-only-once.sh
# Paramètre l'agent View, le fichier d'exemple
# viewagent-custom.conf.template demeure en place
cat <<EOF > /etc/vmware/viewagent-custom.conf
RunOnceScript=/opt/VPSI/scripts/join-only-once.sh
RunOnceScriptTimeout=120
SSODesktopType=UseGnomeUbuntu
OfflineJoinDomain=none
CDREnable=FALSE
EOF
# Redémarre l'agent
systemctl restart viewagent

# Corrige les les PAMs (modifié par l'installation de l'agent View
cauth=/etc/pam.d/common-auth
csession=/etc/pam.d/common-session
printf "{2} Adjusts the PAMs setting\n"
cp -p ${cauth} ${cf}/common-auth.viewagent
cp -p ${csession} ${cf}/common-session.viewagent
# Efface la dernière ligne
tail -n 1 ${cauth} | wc -c | xargs -I {} truncate ${cauth} -s -{}
tail -n 1 ${csession} | wc -c | xargs -I {} truncate ${csession} -s -{}
printf "# This what has to be added to /etc/pam.d/common-auth to " >> ${cauth}
printf "install manage_cred.py\n" >> ${cauth}
printf "auth\toptional\t\t\tpam_exec.so log=/var/log/manage_cred.log " >> ${cauth}
printf "expose_authtok quiet /usr/local/bin/manage_cred.py\n" >> ${cauth}
printf "# end of pam-auth-update config\n" >> ${cauth}

printf "# This what has to be added to /etc/pam.d/common-session " >> ${csession}
printf "to install epfl_roaming.py\n" >> ${csession}
printf "session optional\tpam_exec.so\tlog=/var/log/epfl_roaming2.log " >> ${csession}
printf "/usr/local/bin/epfl_roaming.py --pam\n" >> ${csession}
printf "# end of pam-auth-update config\n" >> ${csession}

# Désactive la liste des utilisateurs au login screen
# L'utilisateur doit alors renseigner son nom d'utilisateur et son mot de passe
printf "{3} Disable user list at login screen\n"
cat <<EOF > /etc/dconf/profile/gdm
user-db:user
system-db:gdm
file-db:/usr/share/gdm/greeter-dconf-defaults
EOF
mkdir /etc/dconf/db/gdm.d
cat <<EOF > /etc/dconf/db/gdm.d/00-login-screen
[org/gnome/login-screen]
# Do not show the user list
disable-user-list=true
EOF
dconf update
# Active la clé org.gnome.login-screen.disable-user-list
# Crée le profile gdm dans le fichier gdm qui est la base de données dconf
# Crée le keyfile gdm pour l'ensemble de la machine en plaçant les
# configuration dans /etc/dconf/db/gdm.d/00-login-screen

# Supprime la boîte de dialogue de confirmation des actions
# logout, restart et shutdown
printf "{4} Suppress the dialog to confirm logout, restart and shutdown action\n"
cat <<EOF > /usr/share/glib-2.0/schemas/10_com.canonical.indicator.session.gschema.override
[com.canonical.indicator.session]
suppress-logout-restart-shutdown=true
EOF
glib-compile-schemas /usr/share/glib-2.0/schemas/

printf "\n"
printf "Manually:\n"
printf "  * Add printer queues to myPrint.\n"
printf "  * Unbind the machine from AD (aka Active Directory)\n"
printf "  * Update message of the day (/etc/motd)\n"
printf "\n"
printf "Power-off the machine with the command: 'shutdown -p now; exit'\n\n"
printf 'All is done !\n'

exit 0
#---- end of script ------------------------------------------------------------
