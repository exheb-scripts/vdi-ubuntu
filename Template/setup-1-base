#!/bin/sh
#
# setup-1-base
#
# VDI VMware Horizon Ubuntu 18.04.2, 16.04.5
# 21 mars 2018, Pascal Fabbri
# révision du 16 janvier 2020
#
# À ajouter:
# 16 janvier 2020
#   - Désactivation de la mise à jour automatique, au point 8:
#       /etc/apt/apt.conf.d/20auto-upgrades
#       APT::Periodic::Update-Package-Lists "0";
#
# 15 janvier 2020
#   - Le package winbind est returé, il induit des erreurs lors de la mise à
#     jour du système sur le modèle.
#   - Le package zsh est ajouté.
# 5 septembre 2019
#   - Ajoute les packages de langue manquants.
#   - /etc/motd mis à jour.
# 29 août 2019
#   - Modifications mineurs.
# 4 juillet, 17 juin 2019
#   - Modifications mineurs.
# 4 juin 2019
#   - Modifications pour Ubuntu 18.04
#   - apt install remplacé par apt-get install (quand c'est possible)
#   - /etc/motd mis à jour
#   - La première commande apt sera:
#       apt --yes autoremove
#
# Spécificités d'installation Ubuntu 16.04.5
#   GNU Grub
#     *Install Ubuntu
#   Welcome
#     English
#   Preparing to install Ubuntu
#     [ ] Download updates while installing Ubuntu
#     [ ] Install third-party software ...
#   Installation type
#     (*) Erase disk and install Ubuntu
#   Where are you?
#     Zurich
#   Keyboard layout
#     Switzerland - French (Switzerland)
#     --> Retirer English (US) une fois le système installé.
#   Who are you?
#     Your name: Sys. Admin.
#     Your computer's name: fusion-sysvm/vsphere-sysvm
#       alternate hostname: fusion-sysvm-a, fusion-sysvm-b, fusion-sysvm-a1
#     Pick a username (user name): system
#
# Spécificités d'installation Ubuntu 18.04.2
#   GNU Grub
#     * Install Ubuntu
#   (1/7) Welcome
#     English
#  Install Ubuntu
#  (2/7) Keyboard layout
#    Switzerland - French(Switzerland)
#  (4/7) Updates and other software
#    What apps would you like to install to start with?
#    (*) Normal Installation
#        Web browser, utilities, office software, games, and media players.
#    Other Options
#      [x] Dowload updates while installing Ubuntu
#  (5/7) Installation type
#    This computer currently has no detected operating systems. What would you like to do?
#    (*) Erase disk and install Ubuntu
#        Warning: This wull delete all your programs, ...
#  (6/7) Where are you?
#    Zurich
#  (7/7) Who are you?
#    Your name: Sys. Admin.
#    Your computer's name: vsphere-sysvm-a
#    Pick a username: system
#    (*) Require my password to log in
#
# Notes:
#   - Dupliquer depuis le dépôt les fichier d'installation et
#     lancement du premier script:
#
#       # apt --yes install git-core
#       % git clone https://gitlab.epfl.ch/exheb-scripts/vdi-ubuntu.git
#       % cd vdi-ubuntu
#       % cp .cshrc ..
#       % cp .vimrc ..
#       % cp .gitconfig ..
#       # hostnamectl set-hostname vsphere-sysvm-a ; echo "par exemple"
#       # cd Template
#       # ./setup-1-base |& tee $HOME/vdi-ubuntu/Template/setup-1-base.log
#
# ---- {1/3} -------------------------------------------------------------------

if [ $USER != "root" ]
then
  printf "Run as root !\n"
  exit 1
fi

release=`lsb_release -rs`
case "$release" in
  "18.04")
    ;;
  "16.04")
    ;;
  *)
    printf "Version Ubuntu non reconnue (18.04, 16.04 LTS): %s\n\n" $release
    printf 'Nothing is done !\n'
    exit 2
    ;;
esac

printf "\n{1} Installation des packages nécessaires.\n"

apt --yes autoremove
apt-get --yes install gnome-session-flashback > /dev/null 2>&1
if [ "$?" -ge 1 ]; then
  printf "Installation de package momentanément indisponible.\n\n"
  printf 'Nothing is done !\n'
  exit 3
fi

printf "\n{1.1} VMware Horizon.\n"
apt-get --yes install python-dbus
apt-get --yes install python-gobject
printf "\n{1.2} VMware vSphere.\n"
apt-get --yes install open-vm-tools
apt-get --yes install open-vm-tools-desktop
printf "\n{1.3} Indispensables.\n"
# tcsh 6.20
apt-get --yes install tcsh
apt-get --yes install ncurses-dev
# PostgreSQL
apt-get --yes install libreadline6-dev
apt-get --yes install zlib1g-dev
apt-get --yes install libxml2-dev
# Indispensables
apt-get --yes install openssh-server
apt-get --yes install vim
apt-get --yes install ntp
# Object Storage (Amazon S3)
apt-get --yes install s3fs

printf "\n{2} Correction des locales.\n"
locale="LANG=en_US.UTF-8 LC_NUMERIC=fr_CH.UTF-8 LC_TIME=fr_CH.UTF-8"
locale="${locale} LC_MONETARY=fr_CH.UTF-8 LC_PAPER=fr_CH.UTF-8"
locale="${locale} LC_NAME=fr_CH.UTF-8 LC_ADDRESS=fr_CH.UTF-8"
locale="${locale} LC_TELEPHONE=fr_CH.UTF-8 LC_MEASUREMENT=fr_CH.UTF-8"
locale="${locale} LC_IDENTIFICATION=fr_CH.UTF-8"
# LANGUAGE = (unset)
# LC_ALL = (unset)

locale-gen fr_CH.UTF-8
localectl status
localectl set-locale ${locale}
localectl status

# Ajoute les packages de langue manquants.
apt-get --yes install `check-language-support -l fr_CH`

printf "\n{3} Modification /etc/hosts (Horizon) et autres.\n"
# /etc/hosts
# 127.0.0.1 localhost sys-vm

cat <<EOF > /etc/ntp.conf
# /etc/ntp.conf, configuration for ntpd; see ntp.conf(5) for help
#
# VDI Ubuntu, 2019-07-04.

driftfile /var/lib/ntp/ntp.drift

statistics loopstats peerstats clockstats
filegen loopstats file loopstats type day enable
filegen peerstats file peerstats type day enable
filegen clockstats file clockstats type day enable

pool 128.178.1.1	iburst
pool ntp.metas.ch	iburst
pool 0.ch.pool.ntp.org

restrict -4 default kod notrap nomodify nopeer noquery limited
restrict -6 default kod notrap nomodify nopeer noquery limited

restrict 127.0.0.1
restrict ::1

restrict source notrap nomodify noquery

# end of file
EOF

systemctl restart ntp

printf "\n{4} System Security Services Daemon (SSSD).\n"
# apt-get -y install realmd sssd sssd-tools samba-common krb5-user packagekit samba-common-bin samba-libs adcli
apt-get --yes install sssd
apt-get --yes install samba-common
# Default Kerberos version 5 realm
# En interactif demande un nom de royaume, INTRANET.EPFL.CH dans notre cas.
export DEBIAN_FRONTEND=noninteractive
apt-get --yes install krb5-user
unset DEBIAN_FRONTEND
sed --in-place --expression \
  's/\tdefault_realm\ =\ ATHENA\.MIT\.EDU/\tdefault_realm\ =\ INTRANET\.EPFL\.CH/' \
  /etc/krb5.conf

apt-get --yes install libwbclient-sssd nfs-common cifs-utils autofs autofs-ldap


apt-get --yes install packagekit
# samba-common-bin en principe déjà installé
# apt-get --yes install samba-common-bin
apt-get --yes install samba-dsdb-modules

printf "\n{5} Confort pour l'utilisateur system.\n"
# wbinfo: permet d'interroger le daemon winbind
# Mais il induit des erreurs lors de la mise à jour du modèle.
# apt-get --yes install winbind
apt-get --yes install zsh
usermod -s /usr/bin/tcsh system

directory=~system/.ssh
if [ ! -d $directory ]; then
  mkdir $directory
fi
unset directory

mkdir ~system/.ssh
chmod go-rwx ~system/.ssh
# rhino: clé privé et config
cat <<EOF > ~system/.ssh/authorized_keys
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAOW72tA4AzlU5DCFMUKZB1Ni8/ENnVlZxl1+E4j96Lw VDI VMware Horizon (2018-03-23).
EOF
chmod go-rw ~system/.ssh/authorized_keys
chown -R system:system ~system/.ssh
## cp .../.tcshrc ~system
## cp .../.vimrc ~system
## cat .../key.pub >> .ssh/authorized_keys

printf "\n{6} Welcome message.\n"
cat <<EOF > /etc/motd
                                                            ____________________
                                                            Perpetual Laboratory

                                                  EPFL - Facilities & Operations

                                                              Ubuntu 18.04.3 LTS
                                                              2019.09.05 current

EOF

printf "\n{7} Sécurité\n"
# startup ou shutdow: regénérer les host's keys
# rm /etc/ssh/ssh_host_*
# dpkg-reconfigure openssh-server
# Production des clés de groupe
# ssh-keygen -o -a 128 -t ed25519 -C "VDI VMware Horizon (2018-03-23)." -N "vDI-007" -f VDI-id_ed25519
# ls -1 VDI*
#   VDI-id_ed25519
#   VDI-id_ed25519.pub
# /etc/ssh/sshd_config
#   PasswordAuthentication no
cat << EOF >> /etc/ssh/sshd_config

# VDI VMware Horizon
PasswordAuthentication no
EOF

printf "\n{8} Mise à jour avec tous les correctifs du jour, "
printf "y compris ceux de sécurité.\n"
killall update-manager
apt update
apt list --upgradable
# Quelle est la différence entre upgrade et dist-upgrade ?
apt dist-upgrade --yes
apt autoremove --yes

printf "\n{9} Installation du certificat EPFL\n"
mkdir /usr/share/ca-certificates/epfl
cd /usr/share/ca-certificates/epfl
wget -O EPFL_CA.crt http://certauth.epfl.ch/epflca.pem
printf "epfl/EPFL_CA.crt\n" >> /etc/ca-certificates.conf
cp -p EPFL_CA.crt /etc/ssl/certs/EPFL_CA.pem
cd /etc/ssl/certs/
ln -s EPFL_CA.pem `openssl x509 -hash -noout -in EPFL_CA.pem`.0

update-ca-certificates
# Vérification du fonctionnement du certificat auprès d'Active Directory
# openssl s_client -connect ad1.intranet.epfl.ch:636

printf "\n"
printf "Reboot with the command: 'shutdown -r now; exit'\n\n"
printf "To verify that updates was well done, launch Software Updater after reboot.\n\n"
printf 'All is done !\n'

exit 0
#---- end of script ------------------------------------------------------------
